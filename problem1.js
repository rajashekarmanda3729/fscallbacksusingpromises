const fs = require('fs')

function createAndDeleteRandomJOSNFiles(directoryName, randomFilesCount) {
  const promise = createDirectory()
  
  promise.then((result) => {
    return createRandomJSONFiles()
  }).then((result) => {
    return deleteCreatedRandomJSONFiles()
  }).catch((err) => {
    console.log(err)
  })

  function createDirectory() {
    return new Promise((resolve, reject) => {
      fs.mkdir(directoryName, (err) => {
        if (err) {
          console.log(err)
          reject(err)
        } else {
          resolve(`${directoryName} Directory created successfully`)
        }
      })
    })
  }

  function createRandomJSONFiles() {
    if (randomFilesCount < 1) {
      randomFilesCount = 3
    }
    return new Promise((resolve, reject) => {
      for (let i = 0; i < randomFilesCount; i++) {
        fs.writeFile(`${directoryName}/randomJOSNFile${i + 1}.txt`, 'lipsumData', (err) => {
          if (err) {
            reject(err)
          } else {
            resolve(`${directoryName}/randomJOSNFile${i + 1}.txt JSONFile created successfully`)
          }
        })
      }
    })
  }

  function deleteCreatedRandomJSONFiles() {
    return new Promise((resolve, reject) => {
      for (let i = 0; i < randomFilesCount; i++) {
        fs.unlink(`${directoryName}/randomJOSNFile${i + 1}.txt`, (err) => {
          if (err) {
            reject(err)
          } else {
            resolve(`${directoryName}/randomJOSNFile${i + 1}.txt JSONFile deleted Successfully`)
          }
        })
      }
    })
  }
}
module.exports = createAndDeleteRandomJOSNFiles