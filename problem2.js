const fs = require('fs')

function performActionsOnFiles(filePath) {
    const promise = readAFile()

    promise.then((data) => {
        return convertToUpperCase(data)
    }).then((result) => {
        return createFileaNamesFile(result)
    }).then((data) => {
        return readAFile(data)
    }).then((data) => {
        return convertToLowerCase(data)
    }).then((data) => {
        return readAFile(data)
    }).then((data) => {
        return sortLisumData(data)
    }).then((data) => {
        return readFileNamesFile(data)
    }).then((data) => {
        return deleteFileNamesFiles(data)
    }).catch((err) => {
        console.log(err)
    })

    function readAFile() {
        return new Promise((resolve, reject) => {
            fs.readFile('lipsum.txt', 'utf-8', (err, data) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(data)
                }
            })
        })
    }
    function convertToUpperCase(data) {
        const dataIs = data.toUpperCase()
        return new Promise((resolve, reject) => {
            fs.writeFile(`${filePath}/lipsumUpperCase.txt`, dataIs, (err, data) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(data)
                }
            })
        })
    }
    function createFileaNamesFile() {
        return new Promise((resolve, reject) => {
            fs.writeFile(`${filePath}/fileNames.txt`, `lipsumUpperCase.txt`, (err, data) => {
                if (err) {
                    reject(err)
                } else {
                    resolve('successfully written in fileNames.txt with Uppercase')
                }
            })
        })
    }
    function convertToLowerCase(data) {
        return new Promise((resolve, reject) => {
            fs.writeFile(`${filePath}/lowerCaseLipsum.txt`, data.toLowerCase().split('\n'), (err) => {
                if (err) {
                    reject(err)
                } else {
                    resolve('converted to lowercase')
                }
            })
            fs.appendFile(`${filePath}/fileNames.txt`, '\nlowerCaseLipsum.txt', (err) => {
                if (err) {
                    reject(err)
                } else {
                    resolve('added text to fileNames.txt')
                }
            })
        })
    }
    function sortLisumData(data) {
        return new Promise((resolve, reject) => {
            fs.writeFile(`${filePath}/sortLipsum.txt`, data.split('.').sort(), (err) => {
                if (err) {
                    reject(err)
                } else {
                    resolve('sorted success')
                }
            })
            fs.appendFile(`${filePath}/fileNames.txt`, '\nsortLipsum.txt', (err) => {
                if (err) {
                    reject(err)
                } else {
                    resolve('added text to fileNames.txt')
                }
            })
        })
    }
    function readFileNamesFile() {
        return new Promise((resolve, reject) => {
            fs.readFile(`${filePath}/fileNames.txt`, 'utf-8', (err, data) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(data)
                }
            })
        })
    }
    function deleteFileNamesFiles(data) {
        let fileNamesAll = data.split('\n')
        return new Promise((resolve, reject) => {
            for (let eachFile of fileNamesAll) {
                fs.unlink(`storeData/${eachFile}`,(err) => {
                    if (err) {
                        reject(err)
                    }else {
                        resolve('deleted file '+ eachFile)
                    }
                })
            }
        })

    }
}
module.exports = performActionsOnFiles